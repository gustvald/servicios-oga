package com.oga.springboot.app.personas.models.service;

public interface ICompoundService extends 
ICaracteresService, IDefensoriasService, IDependenciasService,
IIntervinientesService, IPersonasService, IPersonaTiposService,
IResultadosService, IVictimasService, IDependenciaTiposService{

}
