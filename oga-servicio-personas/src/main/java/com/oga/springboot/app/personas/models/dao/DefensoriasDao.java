package com.oga.springboot.app.personas.models.dao;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.oga.springboot.app.personas.models.entity.Defensorias;

public interface DefensoriasDao extends PagingAndSortingRepository<Defensorias, Integer>{

}
