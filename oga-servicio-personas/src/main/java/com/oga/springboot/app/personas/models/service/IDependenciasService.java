package com.oga.springboot.app.personas.models.service;

import java.util.List;

import com.oga.springboot.app.personas.models.entity.Dependencias;

public interface IDependenciasService {
	public List<Dependencias> findDependenciasAll();
	public Dependencias findDependenciasById(Integer id);
	public Dependencias saveDependencias(Dependencias dependencias);
	public void deleteDependenciasById(Integer id);
}
