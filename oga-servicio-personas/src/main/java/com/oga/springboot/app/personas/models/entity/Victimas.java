package com.oga.springboot.app.personas.models.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import com.oga.springboot.app.commons.models.entity.Usuarios;

@Entity
@Table(name = "victimas")
public class Victimas implements Serializable {

	private static final long serialVersionUID = 2795136139627511349L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(nullable = false)
	private Integer id;
	private Integer audienciaid;
	@JoinColumn(name = "personaid", referencedColumnName = "id", nullable = false)
	@ManyToOne(optional = false)
	private Personas personaid;
	@Column(length = 200)
	private String domicilio;
	@Column(length = 200)
	private String email;
	@Column(length = 60)
	private String telefono;
	@Basic(optional = false)
	@Column(nullable = false)
	private short menor;
	@JoinColumn(name = "particularid", referencedColumnName = "id")
	@ManyToOne
	private Intervinientes particularid;
	@JoinColumn(name = "defensoriaid", referencedColumnName = "id")
	@ManyToOne
	private Defensorias defensoriaid;
	@Basic(optional = false)
	@Column(nullable = false)
	private short querellante;
	@Column(length = 250)
	private String observaciones;
	private Integer usuarioid;
	@Transient
	private Usuarios usuario;
	@Column(name = "created_at")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdAt;
	@Column(name = "updated_at")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedAt;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getAudienciaid() {
		return audienciaid;
	}
	public void setAudienciaid(Integer audienciaid) {
		this.audienciaid = audienciaid;
	}
	public Personas getPersonaid() {
		return personaid;
	}
	public void setPersonaid(Personas personaid) {
		this.personaid = personaid;
	}
	public String getDomicilio() {
		return domicilio;
	}
	public void setDomicilio(String domicilio) {
		this.domicilio = domicilio;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public short getMenor() {
		return menor;
	}
	public void setMenor(short menor) {
		this.menor = menor;
	}
	public Intervinientes getParticularid() {
		return particularid;
	}
	public void setParticularid(Intervinientes particularid) {
		this.particularid = particularid;
	}
	public Defensorias getDefensoriaid() {
		return defensoriaid;
	}
	public void setDefensoriaid(Defensorias defensoriaid) {
		this.defensoriaid = defensoriaid;
	}
	public short getQuerellante() {
		return querellante;
	}
	public void setQuerellante(short querellante) {
		this.querellante = querellante;
	}
	public String getObservaciones() {
		return observaciones;
	}
	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}
	public Integer getUsuarioid() {
		return usuarioid;
	}
	public void setUsuarioid(Integer usuarioid) {
		this.usuarioid = usuarioid;
	}
	public Usuarios getUsuario() {
		return usuario;
	}
	public void setUsuario(Usuarios usuario) {
		this.usuario = usuario;
	}
	public Date getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}
	public Date getUpdatedAt() {
		return updatedAt;
	}
	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

}
