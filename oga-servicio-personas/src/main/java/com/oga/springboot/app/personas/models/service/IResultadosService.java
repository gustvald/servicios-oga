package com.oga.springboot.app.personas.models.service;

import java.util.List;

import com.oga.springboot.app.personas.models.entity.Resultados;

public interface IResultadosService {
	public List<Resultados> findResultadosAll();
	public Resultados findResultadosById(Integer id);
	public Resultados saveResultados(Resultados resultados);
	public void deleteResultadosById(Integer id);
}
