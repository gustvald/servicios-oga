package com.oga.springboot.app.personas.models.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "intervinientes")
public class Intervinientes implements Serializable{

	private static final long serialVersionUID = -3671847451733365945L;
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
	private Integer audienciaid;
    @JoinColumn(name = "personaid", referencedColumnName = "id", nullable = false)
    @ManyToOne(optional = false)
    private Personas personaid;
    @JoinColumn(name = "caracterid", referencedColumnName = "id", nullable = false)
    @ManyToOne(optional = false)
    private Caracteres caracterid;
    @Column(length = 255)
    private String email;
    @Column(length = 60)
    private String telefono;
    @Column(length = 150)
    private String domicilio;
    private Short detenido;
    @JoinColumn(name = "dependenciaid", referencedColumnName = "id")
    @ManyToOne
    private Dependencias dependenciaid;
    @Column(name = "fecha_detencion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaDetencion;
    @Column(name = "fecha_vencimiento")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaVencimiento;
    @JoinColumn(name = "resultadoid", referencedColumnName = "id")
    @ManyToOne
    private Resultados resultadoid;
    @Column(name = "resultado_obs", length = 200)
    private String resultadoObs;
    @Column(name = "created_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;
    @Column(name = "updated_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedAt;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getAudienciaid() {
		return audienciaid;
	}
	public void setAudienciaid(Integer audienciaid) {
		this.audienciaid = audienciaid;
	}
	public Personas getPersonaid() {
		return personaid;
	}
	public void setPersonaid(Personas personaid) {
		this.personaid = personaid;
	}
	public Caracteres getCaracterid() {
		return caracterid;
	}
	public void setCaracterid(Caracteres caracterid) {
		this.caracterid = caracterid;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public String getDomicilio() {
		return domicilio;
	}
	public void setDomicilio(String domicilio) {
		this.domicilio = domicilio;
	}
	public Short getDetenido() {
		return detenido;
	}
	public void setDetenido(Short detenido) {
		this.detenido = detenido;
	}
	public Dependencias getDependenciaid() {
		return dependenciaid;
	}
	public void setDependenciaid(Dependencias dependenciaid) {
		this.dependenciaid = dependenciaid;
	}
	public Date getFechaDetencion() {
		return fechaDetencion;
	}
	public void setFechaDetencion(Date fechaDetencion) {
		this.fechaDetencion = fechaDetencion;
	}
	public Date getFechaVencimiento() {
		return fechaVencimiento;
	}
	public void setFechaVencimiento(Date fechaVencimiento) {
		this.fechaVencimiento = fechaVencimiento;
	}
	public Resultados getResultadoid() {
		return resultadoid;
	}
	public void setResultadoid(Resultados resultadoid) {
		this.resultadoid = resultadoid;
	}
	public String getResultadoObs() {
		return resultadoObs;
	}
	public void setResultadoObs(String resultadoObs) {
		this.resultadoObs = resultadoObs;
	}
	public Date getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}
	public Date getUpdatedAt() {
		return updatedAt;
	}
	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}
    
}
