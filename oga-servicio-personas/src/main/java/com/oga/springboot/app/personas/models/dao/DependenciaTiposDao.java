package com.oga.springboot.app.personas.models.dao;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.oga.springboot.app.personas.models.entity.DependenciaTipos;

public interface DependenciaTiposDao extends PagingAndSortingRepository<DependenciaTipos, Integer>{

}
