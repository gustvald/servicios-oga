package com.oga.springboot.app.personas.models.service;

import java.util.List;

import com.oga.springboot.app.personas.models.entity.Intervinientes;

public interface IIntervinientesService {
	public List<Intervinientes> findIntervinientesAll();
	public Intervinientes findIntervinientesById(Integer id);
	public Intervinientes saveIntervinientes(Intervinientes intervinientes);
	public void deleteIntervinientesById(Integer id);
}
