package com.oga.springboot.app.personas.models.dao;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.oga.springboot.app.personas.models.entity.PersonaTipos;

public interface PersonaTiposDao extends PagingAndSortingRepository<PersonaTipos, Integer>{

}
