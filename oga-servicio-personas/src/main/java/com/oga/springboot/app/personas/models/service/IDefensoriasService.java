package com.oga.springboot.app.personas.models.service;

import java.util.List;

import com.oga.springboot.app.personas.models.entity.Defensorias;

public interface IDefensoriasService {
	public List<Defensorias> findDefensoriasAll();
	public Defensorias findDefensoriasById(Integer id);
	public Defensorias saveDefensorias(Defensorias defensorias);
	public void deleteDefensoriasById(Integer id);
	public Defensorias findDefensoriasCompletoById(Integer id);
}
