package com.oga.springboot.app.personas.models.service;

import java.util.List;

import com.oga.springboot.app.personas.models.entity.Personas;

public interface IPersonasService {
	public List<Personas> findPersonasAll();
	public Personas findPersonasById(Integer id);
	public Personas savePersonas(Personas personas);
	public void deletePersonasById(Integer id);
	public Personas findPersonasCompletoById(Integer id);
}
