package com.oga.springboot.app.personas.models.service;

import java.util.List;

import com.oga.springboot.app.personas.models.entity.PersonaTipos;

public interface IPersonaTiposService {
	public List<PersonaTipos> findPersonaTiposAll();
	public PersonaTipos findPersonaTiposById(Integer id);
	public PersonaTipos savePersonaTipos(PersonaTipos personaTipos);
	public void deletePersonaTiposById(Integer id);
}
