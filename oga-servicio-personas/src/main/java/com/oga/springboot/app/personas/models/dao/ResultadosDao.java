package com.oga.springboot.app.personas.models.dao;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.oga.springboot.app.personas.models.entity.Resultados;

public interface ResultadosDao extends PagingAndSortingRepository<Resultados, Integer>{

}
