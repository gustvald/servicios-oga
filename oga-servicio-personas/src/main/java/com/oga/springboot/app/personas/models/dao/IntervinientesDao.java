package com.oga.springboot.app.personas.models.dao;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.oga.springboot.app.personas.models.entity.Intervinientes;

public interface IntervinientesDao extends PagingAndSortingRepository<Intervinientes, Integer>{

}
