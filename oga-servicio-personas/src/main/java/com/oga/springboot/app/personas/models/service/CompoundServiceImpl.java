package com.oga.springboot.app.personas.models.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.oga.springboot.app.commons.models.entity.Usuarios;
import com.oga.springboot.app.personas.clients.UsuariosClienteRest;
import com.oga.springboot.app.personas.models.dao.CaracteresDao;
import com.oga.springboot.app.personas.models.dao.DefensoriasDao;
import com.oga.springboot.app.personas.models.dao.DependenciaTiposDao;
import com.oga.springboot.app.personas.models.dao.DependenciasDao;
import com.oga.springboot.app.personas.models.dao.IntervinientesDao;
import com.oga.springboot.app.personas.models.dao.PersonaTiposDao;
import com.oga.springboot.app.personas.models.dao.PersonasDao;
import com.oga.springboot.app.personas.models.dao.ResultadosDao;
import com.oga.springboot.app.personas.models.dao.VictimasDao;
import com.oga.springboot.app.personas.models.entity.Caracteres;
import com.oga.springboot.app.personas.models.entity.Defensorias;
import com.oga.springboot.app.personas.models.entity.DependenciaTipos;
import com.oga.springboot.app.personas.models.entity.Dependencias;
import com.oga.springboot.app.personas.models.entity.Intervinientes;
import com.oga.springboot.app.personas.models.entity.PersonaTipos;
import com.oga.springboot.app.personas.models.entity.Personas;
import com.oga.springboot.app.personas.models.entity.Resultados;
import com.oga.springboot.app.personas.models.entity.Victimas;

@Service
@Primary
public class CompoundServiceImpl implements ICompoundService{
	
	@Autowired
	private UsuariosClienteRest clienteFeign;
	
	@Autowired
	private CaracteresDao caracteresDao;
	@Autowired
	private DefensoriasDao defensoriasDao;
	@Autowired
	private DependenciasDao dependenciasDao;
	@Autowired
	private DependenciaTiposDao dependenciaTiposDao;
	@Autowired
	private IntervinientesDao intervinientesDao;
	@Autowired
	private PersonasDao personasDao;
	@Autowired
	private PersonaTiposDao personaTiposDao;
	@Autowired
	private ResultadosDao resultadosDao;
	@Autowired
	private VictimasDao victimasDao;
	
	
	@Override
	@Transactional(readOnly=true)
	public List<Caracteres> findCaracteresAll() {
		return (List<Caracteres>) caracteresDao.findAll();
	}

	@Override
	public Caracteres findCaracteresById(Integer id) {
		return caracteresDao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public Caracteres saveCaracteres(Caracteres caracteres) {
		return caracteresDao.save(caracteres);
	}

	@Override
	@Transactional
	public void deleteCaracteresById(Integer id) {
		caracteresDao.deleteById(id);		
	}

	@Override
	@Transactional(readOnly=true)
	public List<Defensorias> findDefensoriasAll() {
		return (List<Defensorias>) defensoriasDao.findAll();
	}

	@Override
	public Defensorias findDefensoriasById(Integer id) {
		return defensoriasDao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public Defensorias saveDefensorias(Defensorias defensorias) {
		return defensoriasDao.save(defensorias);
	}

	@Override
	@Transactional
	public void deleteDefensoriasById(Integer id) {
		defensoriasDao.deleteById(id);		
	}

	@Override
	@Transactional(readOnly=true)
	public List<Dependencias> findDependenciasAll() {
		return (List<Dependencias>) dependenciasDao.findAll();
	}

	@Override
	public Dependencias findDependenciasById(Integer id) {
		return dependenciasDao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public Dependencias saveDependencias(Dependencias dependencias) {
		return dependenciasDao.save(dependencias);
	}

	@Override
	@Transactional
	public void deleteDependenciasById(Integer id) {
		dependenciasDao.deleteById(id);		
	}

	@Override
	@Transactional(readOnly=true)
	public List<Intervinientes> findIntervinientesAll() {
		return (List<Intervinientes>) intervinientesDao.findAll();
	}

	@Override
	public Intervinientes findIntervinientesById(Integer id) {
		return intervinientesDao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public Intervinientes saveIntervinientes(Intervinientes intervinientes) {
		return intervinientesDao.save(intervinientes);
	}

	@Override
	@Transactional
	public void deleteIntervinientesById(Integer id) {
		intervinientesDao.deleteById(id);		
	}

	@Override
	@Transactional(readOnly=true)
	public List<Personas> findPersonasAll() {
		return (List<Personas>) personasDao.findAll();
	}

	@Override
	public Personas findPersonasById(Integer id) {
		return personasDao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public Personas savePersonas(Personas personas) {
		return personasDao.save(personas);
	}

	@Override
	@Transactional
	public void deletePersonasById(Integer id) {
		personasDao.deleteById(id);		
	}

	@Override
	@Transactional(readOnly=true)
	public List<PersonaTipos> findPersonaTiposAll() {
		return (List<PersonaTipos>) personaTiposDao.findAll();
	}

	@Override
	public PersonaTipos findPersonaTiposById(Integer id) {
		return personaTiposDao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public PersonaTipos savePersonaTipos(PersonaTipos personaTipos) {
		return personaTiposDao.save(personaTipos);
	}

	@Override
	@Transactional
	public void deletePersonaTiposById(Integer id) {
		personaTiposDao.deleteById(id);		
	}

	@Override
	@Transactional(readOnly=true)
	public List<Resultados> findResultadosAll() {
		return (List<Resultados>) resultadosDao.findAll();
	}

	@Override
	public Resultados findResultadosById(Integer id) {
		return resultadosDao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public Resultados saveResultados(Resultados resultados) {
		return resultadosDao.save(resultados);
	}

	@Override
	@Transactional
	public void deleteResultadosById(Integer id) {
		resultadosDao.deleteById(id);		
	}

	@Override
	@Transactional(readOnly=true)
	public List<Victimas> findVictimasAll() {
		return (List<Victimas>) victimasDao.findAll();
	}

	@Override
	public Victimas findVictimasById(Integer id) {
		return victimasDao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public Victimas saveVictimas(Victimas victimas) {
		return victimasDao.save(victimas);
	}

	@Override
	@Transactional
	public void deleteVictimasById(Integer id) {
		victimasDao.deleteById(id);		
	}

	@Override
	@Transactional(readOnly=true)
	public List<DependenciaTipos> findDependenciaTiposAll() {
		return (List<DependenciaTipos>) dependenciaTiposDao.findAll();
	}

	@Override
	public DependenciaTipos findDependenciaTiposById(Integer id) {
		return dependenciaTiposDao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public DependenciaTipos saveDependenciaTipos(DependenciaTipos dependenciaTipos) {
		return dependenciaTiposDao.save(dependenciaTipos);
	}

	@Override
	@Transactional
	public void deleteDependenciaTiposById(Integer id) {
		dependenciaTiposDao.deleteById(id);		
	}

	@Override
	public Personas findPersonasCompletoById(Integer id) {
		Personas persona = personasDao.findById(id).orElse(null);
		if (persona!=null && persona.getUsuarioid()!=null) {
			Usuarios usuario = clienteFeign.detalle(persona.getUsuarioid());
			persona.setUsuario(usuario);
		}
		return persona;
	}

	@Override
	public Defensorias findDefensoriasCompletoById(Integer id) {
		Defensorias defensoria = defensoriasDao.findById(id).orElse(null);
		if (defensoria!=null && defensoria.getUsuarioid()!=null) {
			Usuarios usuario = clienteFeign.detalle(defensoria.getUsuarioid());
			defensoria.setUsuario(usuario);
		}
		return defensoria;
	}

	@Override
	public Victimas findVictimasCompletoById(Integer id) {
		Victimas victima = victimasDao.findById(id).orElse(null);
		if (victima!=null && victima.getUsuarioid()!=null) {
			Usuarios usuario = clienteFeign.detalle(victima.getUsuarioid());
			victima.setUsuario(usuario);
		}
		return victima;
	}
	
}
