package com.oga.springboot.app.personas.models.dao;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.oga.springboot.app.personas.models.entity.Dependencias;

public interface DependenciasDao extends PagingAndSortingRepository<Dependencias, Integer>{

}
