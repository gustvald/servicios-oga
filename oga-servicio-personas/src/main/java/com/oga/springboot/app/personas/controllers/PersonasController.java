package com.oga.springboot.app.personas.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.oga.springboot.app.personas.models.entity.Caracteres;
import com.oga.springboot.app.personas.models.entity.Defensorias;
import com.oga.springboot.app.personas.models.entity.DependenciaTipos;
import com.oga.springboot.app.personas.models.entity.Dependencias;
import com.oga.springboot.app.personas.models.entity.Intervinientes;
import com.oga.springboot.app.personas.models.entity.PersonaTipos;
import com.oga.springboot.app.personas.models.entity.Personas;
import com.oga.springboot.app.personas.models.entity.Resultados;
import com.oga.springboot.app.personas.models.entity.Victimas;
import com.oga.springboot.app.personas.models.service.ICompoundService;

@RestController
public class PersonasController {
	
	@Autowired
	ICompoundService compoundService;
	
	@GetMapping("/listar-caracteres")
	public List<Caracteres> listarCaracteres(){
		return compoundService.findCaracteresAll();
	}
	
	@GetMapping("/ver-caracteres/{id}")
	public Caracteres detalleCaracter(@PathVariable Integer id){
		return  compoundService.findCaracteresById(id);
	}
	
	@PostMapping("/crear-caracteres")
	@ResponseStatus(HttpStatus.CREATED)
	public Caracteres crearCaracteres(@RequestBody Caracteres caracteres) {
		return compoundService.saveCaracteres(caracteres);
	}
	
	@PutMapping("/editar-caracteres/{id}")
	@ResponseStatus(HttpStatus.CREATED)
	public Caracteres editarCaracteres(@RequestBody Caracteres caracteres, @PathVariable Integer id) {
		Caracteres caracteresDb = compoundService.findCaracteresById(id);
		return compoundService.saveCaracteres(caracteresDb);
	}
	
	@DeleteMapping("/eliminar-caracteres/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void eliminarCaracteres(@PathVariable Integer id) {
		compoundService.deleteCaracteresById(id);
	}
	
	@GetMapping("/listar-defensorias")
	public List<Defensorias> listarDefensorias(){
		return compoundService.findDefensoriasAll();
	}
	
	@GetMapping("/ver-defensorias/{id}")
	public Defensorias detalleDefensorias(@PathVariable Integer id){
		return  compoundService.findDefensoriasById(id);
	}
	
	@PostMapping("/crear-defensorias")
	@ResponseStatus(HttpStatus.CREATED)
	public Defensorias crearDefensorias(@RequestBody Defensorias defensorias) {
		return compoundService.saveDefensorias(defensorias);
	}
	
	@PutMapping("/editar-defensorias/{id}")
	@ResponseStatus(HttpStatus.CREATED)
	public Defensorias editarDefensorias(@RequestBody Defensorias defensorias, @PathVariable Integer id) {
		Defensorias defensoriasDb = compoundService.findDefensoriasById(id);
		return compoundService.saveDefensorias(defensoriasDb);
	}
	
	@DeleteMapping("/eliminar-defensorias/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void eliminarDefensorias(@PathVariable Integer id) {
		compoundService.deleteDefensoriasById(id);
	}
	
	@GetMapping("/listar-dependencias")
	public List<Dependencias> listarDependencias(){
		return compoundService.findDependenciasAll();
	}
	
	@GetMapping("/ver-dependencias/{id}")
	public Dependencias detalleDependencias(@PathVariable Integer id){
		return  compoundService.findDependenciasById(id);
	}
	
	@PostMapping("/crear-dependencias")
	@ResponseStatus(HttpStatus.CREATED)
	public Dependencias crearDependencias(@RequestBody Dependencias dependencias) {
		return compoundService.saveDependencias(dependencias);
	}
	
	@PutMapping("/editar-dependencias/{id}")
	@ResponseStatus(HttpStatus.CREATED)
	public Dependencias editarDependencias(@RequestBody Dependencias dependencias, @PathVariable Integer id) {
		Dependencias dependenciasDb = compoundService.findDependenciasById(id);
		return compoundService.saveDependencias(dependenciasDb);
	}
	
	@DeleteMapping("/eliminar-dependencias/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void eliminarDependencias(@PathVariable Integer id) {
		compoundService.deleteDependenciasById(id);
	}
	
	@GetMapping("/listar-dependencia-tipo")
	public List<DependenciaTipos> listarDependenciaTipos(){
		return compoundService.findDependenciaTiposAll();
	}
	
	@GetMapping("/ver-dependencia-tipo/{id}")
	public DependenciaTipos detalleDependenciaTipos(@PathVariable Integer id){
		return  compoundService.findDependenciaTiposById(id);
	}
	
	@PostMapping("/crear-dependencia-tipo")
	@ResponseStatus(HttpStatus.CREATED)
	public DependenciaTipos crearDependenciaTipos(@RequestBody DependenciaTipos dependenciaTipos) {
		return compoundService.saveDependenciaTipos(dependenciaTipos);
	}
	
	@PutMapping("/editar-dependencia-tipo/{id}")
	@ResponseStatus(HttpStatus.CREATED)
	public DependenciaTipos editarDependenciaTipos(@RequestBody DependenciaTipos dependenciaTipos, @PathVariable Integer id) {
		DependenciaTipos dependenciaTiposDb = compoundService.findDependenciaTiposById(id);
		return compoundService.saveDependenciaTipos(dependenciaTiposDb);
	}
	
	@DeleteMapping("/eliminar-dependencia-tipo/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void eliminarDependenciaTipos(@PathVariable Integer id) {
		compoundService.deleteDependenciaTiposById(id);
	}
	
	@GetMapping("/listar-intervinientes")
	public List<Intervinientes> listarIntervinientes(){
		return compoundService.findIntervinientesAll();
	}
	
	@GetMapping("/ver-intervinientes/{id}")
	public Intervinientes detalleIntervinientes(@PathVariable Integer id){
		return  compoundService.findIntervinientesById(id);
	}
	
	@PostMapping("/crear-intervinientes")
	@ResponseStatus(HttpStatus.CREATED)
	public Intervinientes crearIntervinientes(@RequestBody Intervinientes intervinientes) {
		return compoundService.saveIntervinientes(intervinientes);
	}
	
	@PutMapping("/editar-intervinientes/{id}")
	@ResponseStatus(HttpStatus.CREATED)
	public Intervinientes editarIntervinientes(@RequestBody Intervinientes intervinientes, @PathVariable Integer id) {
		Intervinientes intervinientesDb = compoundService.findIntervinientesById(id);
		return compoundService.saveIntervinientes(intervinientesDb);
	}
	
	@DeleteMapping("/eliminar-intervinientes/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void eliminarIntervinientes(@PathVariable Integer id) {
		compoundService.deleteIntervinientesById(id);
	}
	
	@GetMapping("/listar-personas")
	public List<Personas> listarPersonas(){
		return compoundService.findPersonasAll();
	}
	
	@GetMapping("/ver-personas/{id}")
	public Personas detallePersonas(@PathVariable Integer id){
		return  compoundService.findPersonasById(id);
	}
	
	@GetMapping("/ver-personas-completo/{id}")
	public Personas detallePersonasCompleto(@PathVariable Integer id){
		return  compoundService.findPersonasCompletoById(id);
	}
	
	@PostMapping("/crear-personas")
	@ResponseStatus(HttpStatus.CREATED)
	public Personas crearPersonas(@RequestBody Personas personas) {
		return compoundService.savePersonas(personas);
	}
	
	@PutMapping("/editar-personas/{id}")
	@ResponseStatus(HttpStatus.CREATED)
	public Personas editarPersonas(@RequestBody Personas personas, @PathVariable Integer id) {
		Personas personasDb = compoundService.findPersonasById(id);
		return compoundService.savePersonas(personasDb);
	}
	
	@DeleteMapping("/eliminar-personas/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void eliminarPersonas(@PathVariable Integer id) {
		compoundService.deletePersonasById(id);
	}


	
	@GetMapping("/listar-persona-tipo")
	public List<PersonaTipos> listarPersonaTipos(){
		return compoundService.findPersonaTiposAll();
	}
	
	@GetMapping("/ver-persona-tipo/{id}")
	public PersonaTipos detallePersonaTipos(@PathVariable Integer id){
		return  compoundService.findPersonaTiposById(id);
	}
	
	@PostMapping("/crear-persona-tipo")
	@ResponseStatus(HttpStatus.CREATED)
	public PersonaTipos crearPersonaTipos(@RequestBody PersonaTipos personaTipos) {
		return compoundService.savePersonaTipos(personaTipos);
	}
	
	@PutMapping("/editar-persona-tipo/{id}")
	@ResponseStatus(HttpStatus.CREATED)
	public PersonaTipos editarPersonaTipos(@RequestBody PersonaTipos personaTipos, @PathVariable Integer id) {
		PersonaTipos personaTiposDb = compoundService.findPersonaTiposById(id);
		return compoundService.savePersonaTipos(personaTiposDb);
	}
	
	@DeleteMapping("/eliminar-persona-tipo/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void eliminarPersonaTipos(@PathVariable Integer id) {
		compoundService.deletePersonaTiposById(id);
	}
	
	
	
	@GetMapping("/listar-resultados")
	public List<Resultados> listarResultados(){
		return compoundService.findResultadosAll();
	}
	
	@GetMapping("/ver-resultados/{id}")
	public Resultados detalleResultados(@PathVariable Integer id){
		return  compoundService.findResultadosById(id);
	}
	
	@PostMapping("/crear-resultados")
	@ResponseStatus(HttpStatus.CREATED)
	public Resultados crearResultados(@RequestBody Resultados resultados) {
		return compoundService.saveResultados(resultados);
	}
	
	@PutMapping("/editar-resultados/{id}")
	@ResponseStatus(HttpStatus.CREATED)
	public Resultados editarResultados(@RequestBody Resultados resultados, @PathVariable Integer id) {
		Resultados resultadosDb = compoundService.findResultadosById(id);
		return compoundService.saveResultados(resultadosDb);
	}
	
	@DeleteMapping("/eliminar-resultados/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void eliminarResultados(@PathVariable Integer id) {
		compoundService.deleteResultadosById(id);
	}
	
	
	@GetMapping("/listar-victimas")
	public List<Victimas> listarVictimas(){
		return compoundService.findVictimasAll();
	}
	
	@GetMapping("/ver-victimas/{id}")
	public Victimas detalleVictimas(@PathVariable Integer id){
		return  compoundService.findVictimasById(id);
	}
	
	@PostMapping("/crear-victimas")
	@ResponseStatus(HttpStatus.CREATED)
	public Victimas crearVictimas(@RequestBody Victimas victimas) {
		return compoundService.saveVictimas(victimas);
	}
	
	@PutMapping("/editar-victimas/{id}")
	@ResponseStatus(HttpStatus.CREATED)
	public Victimas editarVictimas(@RequestBody Victimas victimas, @PathVariable Integer id) {
		Victimas victimasDb = compoundService.findVictimasById(id);
		return compoundService.saveVictimas(victimasDb);
	}
	
	@DeleteMapping("/eliminar-victimas/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void eliminarVictimas(@PathVariable Integer id) {
		compoundService.deleteVictimasById(id);
	}

}
