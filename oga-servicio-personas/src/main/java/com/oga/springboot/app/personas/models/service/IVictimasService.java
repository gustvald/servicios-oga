package com.oga.springboot.app.personas.models.service;

import java.util.List;

import com.oga.springboot.app.personas.models.entity.Victimas;

public interface IVictimasService {
	public List<Victimas> findVictimasAll();
	public Victimas findVictimasById(Integer id);
	public Victimas saveVictimas(Victimas victimas);
	public void deleteVictimasById(Integer id);
	public Victimas findVictimasCompletoById(Integer id);
}
