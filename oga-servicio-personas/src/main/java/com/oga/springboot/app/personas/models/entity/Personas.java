package com.oga.springboot.app.personas.models.entity;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.oga.springboot.app.commons.models.entity.Usuarios;

@Entity
@Table(name = "personas")
public class Personas implements Serializable {

	private static final long serialVersionUID = -3558893824606779659L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(nullable = false)
	private Integer id;
	@Basic(optional = false)
	@Column(nullable = false, length = 120)
	private String apellido;
	@Basic(optional = false)
	@Column(nullable = false, length = 120)
	private String nombre;
	@Column(name = "tipo_documento", length = 30)
	private String tipoDocumento;
	private BigInteger identificacion;
	@Column(length = 255)
	private String email;
	@Column(length = 60)
	private String telefono;
	@Column(length = 250)
	private String domicilio;
//	@JoinColumn(name = "tipoid", referencedColumnName = "id", nullable = false)
//	@ManyToOne(optional = false)
//	@Basic(optional = false)
//	@Column(nullable = false)
//	private PersonaTipos tipoid;
	@Basic(optional = false)
	@Column(nullable = false)
	private short activo;
	private Integer invenietid;
	private Integer usuarioid;
	@Transient
	private Usuarios usuario;
	@Column(name = "created_at")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdAt;
	@Column(name = "updated_at")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedAt;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getApellido() {
		return apellido;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getTipoDocumento() {
		return tipoDocumento;
	}
	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}
	public BigInteger getIdentificacion() {
		return identificacion;
	}
	public void setIdentificacion(BigInteger identificacion) {
		this.identificacion = identificacion;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public String getDomicilio() {
		return domicilio;
	}
	public void setDomicilio(String domicilio) {
		this.domicilio = domicilio;
	}
//	public PersonaTipos getTipoid() {
//		return tipoid;
//	}
//	public void setTipoid(PersonaTipos tipoid) {
//		this.tipoid = tipoid;
//	}
	public short getActivo() {
		return activo;
	}
	public void setActivo(short activo) {
		this.activo = activo;
	}
	public Integer getInvenietid() {
		return invenietid;
	}
	public void setInvenietid(Integer invenietid) {
		this.invenietid = invenietid;
	}
	public Integer getUsuarioid() {
		return usuarioid;
	}
	public void setUsuarioid(Integer usuarioid) {
		this.usuarioid = usuarioid;
	}
	public Usuarios getUsuario() {
		return usuario;
	}
	public void setUsuario(Usuarios usuario) {
		this.usuario = usuario;
	}
	public Date getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}
	public Date getUpdatedAt() {
		return updatedAt;
	}
	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}
	
}
