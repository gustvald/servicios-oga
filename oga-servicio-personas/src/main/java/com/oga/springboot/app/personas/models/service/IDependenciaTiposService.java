package com.oga.springboot.app.personas.models.service;

import java.util.List;

import com.oga.springboot.app.personas.models.entity.DependenciaTipos;

public interface IDependenciaTiposService {
	public List<DependenciaTipos> findDependenciaTiposAll();
	public DependenciaTipos findDependenciaTiposById(Integer id);
	public DependenciaTipos saveDependenciaTipos(DependenciaTipos dependenciaTipos);
	public void deleteDependenciaTiposById(Integer id);
}
