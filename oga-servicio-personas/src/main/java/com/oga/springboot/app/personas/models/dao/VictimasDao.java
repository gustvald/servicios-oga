package com.oga.springboot.app.personas.models.dao;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.oga.springboot.app.personas.models.entity.Victimas;

public interface VictimasDao extends PagingAndSortingRepository<Victimas, Integer>{

}
