package com.oga.springboot.app.personas.clients;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.oga.springboot.app.commons.models.entity.Usuarios;


@FeignClient(name = "servicio-oga-usuarios")
public interface UsuariosClienteRest {
	@GetMapping("/listar-usuarios")
	public List<Usuarios> listar();
	
	@GetMapping("/ver-usuario/{id}")
	public Usuarios detalle(@PathVariable Integer id);
	
	@PostMapping("/crear-usuario")
	public Usuarios crear(@RequestBody Usuarios usuario);
	
	@PutMapping("/editar-usuario/{id}")
	public Usuarios update(@RequestBody Usuarios usuario, @PathVariable Integer id);
	
	@DeleteMapping("/eliminar-usuario/{id}")
	public void eliminar(@PathVariable Integer id);
}
