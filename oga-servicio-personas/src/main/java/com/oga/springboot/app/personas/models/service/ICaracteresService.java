package com.oga.springboot.app.personas.models.service;

import java.util.List;

import com.oga.springboot.app.personas.models.entity.Caracteres;

public interface ICaracteresService {
	public List<Caracteres> findCaracteresAll();
	public Caracteres findCaracteresById(Integer id);
	public Caracteres saveCaracteres(Caracteres caracteres);
	public void deleteCaracteresById(Integer id);
}
