package com.oga.springboot.app.personas.models.dao;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.oga.springboot.app.personas.models.entity.Caracteres;

public interface CaracteresDao extends PagingAndSortingRepository<Caracteres, Integer>{

}
