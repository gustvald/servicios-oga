package com.oga.springboot.app.usuarios.models.dao;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.oga.springboot.app.usuarios.models.entity.Niveles;


public interface NivelesDao extends PagingAndSortingRepository<Niveles, Integer>{

}
