package com.oga.springboot.app.usuarios.models.service;

import java.util.List;

import com.oga.springboot.app.usuarios.models.entity.Niveles;

public interface INivelesService {

	public List<Niveles> findNivelesAll();
	public Niveles findNivelesById(Integer id);
	public Niveles saveNiveles(Niveles nivel);
	public void deleteNivelesById(Integer id);
}
