package com.oga.springboot.app.usuarios.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.oga.springboot.app.usuarios.models.entity.Fueros;
import com.oga.springboot.app.usuarios.models.entity.Niveles;
import com.oga.springboot.app.usuarios.models.entity.Perfiles;
import com.oga.springboot.app.usuarios.models.entity.Unidades;
import com.oga.springboot.app.usuarios.models.entity.Usuarios;
import com.oga.springboot.app.usuarios.models.service.ICompoundService;

@RestController
public class UsuariosController {
	
	@Autowired
	private ICompoundService compoundService;
	
	@GetMapping("/listar-usuarios")
	public List<Usuarios> listarUsuarios(){
		return compoundService.findUsuariosAll();
	}
	
	@GetMapping("/ver-usuario/{id}")
	public Usuarios detalleUsuarios(@PathVariable Integer id){
		return  compoundService.findUsuariosById(id);
	}
	
	@GetMapping("/ver-usuario-completo/{id}")
	public Usuarios detalleCompletoUsuarios(@PathVariable Integer id){
		return  compoundService.findCompleteUsuarioById(id);
	}
	
	
	@PostMapping("/crear-usuario")
	@ResponseStatus(HttpStatus.CREATED)
	public Usuarios crearUsuarios(@RequestBody Usuarios usuarios) {
		return compoundService.saveUsuarios(usuarios);
	}
	
	@PutMapping("/editar-usuario/{id}")
	@ResponseStatus(HttpStatus.CREATED)
	public Usuarios editarSala(@RequestBody Usuarios usuario, @PathVariable Integer id) {
		Usuarios usuarioDb = compoundService.findUsuariosById(id);
		usuarioDb.setActivo(usuario.getActivo());
		usuarioDb.setApellido(usuario.getApellido());
		usuarioDb.setCc(usuario.getCc());
		usuarioDb.setEmail(usuario.getEmail());
		usuarioDb.setLegajo(usuario.getLegajo());
		usuarioDb.setNivelid(usuario.getNivelid());
		usuarioDb.setNombre(usuario.getNombre());
		usuarioDb.setOfijuid(usuario.getOfijuid());
		usuarioDb.setPassword(usuario.getPassword());
		usuarioDb.setPerfilid(usuario.getPerfilid());
		usuarioDb.setPrivadas(usuario.getPrivadas());
		return compoundService.saveUsuarios(usuarioDb);
	}
	
	@DeleteMapping("/eliminar-usuario/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void eliminarUsuarios(@PathVariable Integer id) {
		compoundService.deleteUsuariosById(id);
	}
	
	
	
	@GetMapping("/listar-unidades")
	public List<Unidades> listarUnidades(){
		return compoundService.findUnidadesAll();
	}
	
	@GetMapping("/ver-unidad/{id}")
	public Unidades detalleUnidad(@PathVariable Integer id){
		return  compoundService.findUnidadesById(id);
	}
	
	
	@PostMapping("/crear-unidad")
	@ResponseStatus(HttpStatus.CREATED)
	public Unidades crearUnidad(@RequestBody Unidades unidad) {
		return compoundService.saveUnidades(unidad);
	}
	
	@PutMapping("/editar-unidad/{id}")
	@ResponseStatus(HttpStatus.CREATED)
	public Unidades editarUnidad(@RequestBody Unidades unidad, @PathVariable Integer id) {
		Unidades unidadDb = compoundService.findUnidadesById(id);
		unidadDb.setActivo(unidad.getActivo());
		unidadDb.setDescripcion(unidad.getDescripcion());
		unidadDb.setEmail(unidad.getEmail());
		unidadDb.setFueroid(unidad.getFueroid());
		unidadDb.setInvenietid(unidad.getInvenietid());
		unidadDb.setNominacion(unidad.getNominacion());
		unidadDb.setPerfilid(unidad.getPerfilid());
		unidadDb.setSaeid(unidad.getSaeid());
		return compoundService.saveUnidades(unidadDb);
	}
	
	@DeleteMapping("/eliminar-unidad/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void eliminarUnidad(@PathVariable Integer id) {
		compoundService.deleteUnidadesById(id);
	}
	
	@GetMapping("/listar-perfiles")
	public List<Perfiles> listarPerfiles(){
		return compoundService.findPerfilesAll();
	}
	
	@GetMapping("/ver-perfil/{id}")
	public Perfiles detallePerfiles(@PathVariable Integer id){
		return  compoundService.findPerfilesById(id);
	}
	
	
	@PostMapping("/crear-perfil")
	@ResponseStatus(HttpStatus.CREATED)
	public Perfiles crearPerfiles(@RequestBody Perfiles perfiles) {
		return compoundService.savePerfiles(perfiles);
	}
	
	@PutMapping("/editar-perfil/{id}")
	@ResponseStatus(HttpStatus.CREATED)
	public Perfiles editarPerfiles(@RequestBody Perfiles perfil, @PathVariable Integer id) {
		Perfiles perfilDb = compoundService.findPerfilesById(id);
		perfilDb.setDescripcion(perfil.getDescripcion());
		return compoundService.savePerfiles(perfilDb);
	}
	
	@DeleteMapping("/eliminar-perfil/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void eliminarPerfil(@PathVariable Integer id) {
		compoundService.deletePerfilesById(id);
	}
	
	@GetMapping("/listar-niveles")
	public List<Niveles> listarNiveles(){
		return compoundService.findNivelesAll();
	}
	
	@GetMapping("/ver-nivel/{id}")
	public Niveles detalleNiveles(@PathVariable Integer id){
		return  compoundService.findNivelesById(id);
	}
	
	
	@PostMapping("/crear-nivel")
	@ResponseStatus(HttpStatus.CREATED)
	public Niveles crearNiveles(@RequestBody Niveles niveles) {
		return compoundService.saveNiveles(niveles);
	}
	
	@PutMapping("/editar-nivel/{id}")
	@ResponseStatus(HttpStatus.CREATED)
	public Niveles editarNiveles(@RequestBody Niveles nivel, @PathVariable Integer id) {
		Niveles nivelDb = compoundService.findNivelesById(id);
		nivelDb.setDescripcion(nivel.getDescripcion());
		return compoundService.saveNiveles(nivelDb);
	}
	
	@DeleteMapping("/eliminar-nivel/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void eliminarNivel(@PathVariable Integer id) {
		compoundService.deleteNivelesById(id);
	}
	
	@GetMapping("/listar-fueros")
	public List<Fueros> listarFueros(){
		return compoundService.findFuerosAll();
	}
	
	@GetMapping("/ver-fuero/{id}")
	public Fueros detalleFueros(@PathVariable Integer id){
		return  compoundService.findFuerosById(id);
	}
	
	
	@PostMapping("/crear-fuero")
	@ResponseStatus(HttpStatus.CREATED)
	public Fueros crearFueros(@RequestBody Fueros fuero) {
		return compoundService.saveFueros(fuero);
	}
	
	@PutMapping("/editar-fuero/{id}")
	@ResponseStatus(HttpStatus.CREATED)
	public Fueros editarFueros(@RequestBody Fueros fuero, @PathVariable Integer id) {
		Fueros fueroDb = compoundService.findFuerosById(id);
		fueroDb.setActivo(fuero.getActivo());
		fueroDb.setBd(fuero.getBd());
		fueroDb.setDescripcion(fuero.getDescripcion());
		fueroDb.setInvenietid(fuero.getInvenietid());
		fueroDb.setIp(fuero.getIp());
		fueroDb.setPuerto(fuero.getIp());
		fueroDb.setVisible(fuero.getVisible());
		return compoundService.saveFueros(fueroDb);
	}

}
