package com.oga.springboot.app.usuarios.models.dao;

import org.springframework.data.repository.CrudRepository;

import com.oga.springboot.app.usuarios.models.entity.Perfiles;


public interface PerfilesDao extends CrudRepository<Perfiles, Integer>{

}
