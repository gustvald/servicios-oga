package com.oga.springboot.app.usuarios.models.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "unidades")
public class Unidades implements Serializable {

	private static final long serialVersionUID = -2346340424154192272L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(nullable = false)
	private Integer id;
	@Basic(optional = false)
	@Column(nullable = false, length = 200)
	private String descripcion;
	private Short nominacion;
	// @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?",
	// message="Invalid email")//if the field contains email address consider using
	// this annotation to enforce field validation
	@Column(length = 200)
	private String email;
	private Integer saeid;
	private Integer invenietid;
	@Basic(optional = false)
	@Column(nullable = false)
	private short activo;
	@Column(name = "created_at")
	private Date createdAt;
	@Column(name = "updated_at")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedAt;
	@JoinColumn(name = "fueroid", referencedColumnName = "id", nullable = false)
	@ManyToOne(optional = false)
	private Fueros fueroid;
	@JoinColumn(name = "perfilid", referencedColumnName = "id", nullable = false)
	@ManyToOne(optional = false)
	private Perfiles perfilid;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public Short getNominacion() {
		return nominacion;
	}
	public void setNominacion(Short nominacion) {
		this.nominacion = nominacion;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Integer getSaeid() {
		return saeid;
	}
	public void setSaeid(Integer saeid) {
		this.saeid = saeid;
	}
	public Integer getInvenietid() {
		return invenietid;
	}
	public void setInvenietid(Integer invenietid) {
		this.invenietid = invenietid;
	}
	public short getActivo() {
		return activo;
	}
	public void setActivo(short activo) {
		this.activo = activo;
	}
	public Date getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}
	public Date getUpdatedAt() {
		return updatedAt;
	}
	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}
	public Fueros getFueroid() {
		return fueroid;
	}
	public void setFueroid(Fueros fueroid) {
		this.fueroid = fueroid;
	}
	public Perfiles getPerfilid() {
		return perfilid;
	}
	public void setPerfilid(Perfiles perfilid) {
		this.perfilid = perfilid;
	}
	
}
