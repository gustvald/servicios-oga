package com.oga.springboot.app.usuarios.models.service;

import java.util.List;

import com.oga.springboot.app.usuarios.models.entity.Fueros;

public interface IFuerosService {
	public List<Fueros> findFuerosAll();
	public Fueros findFuerosById(Integer id);
	public Fueros saveFueros(Fueros fuero);
	public void deleteFuerosById(Integer id);
}
