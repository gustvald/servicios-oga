package com.oga.springboot.app.usuarios.models.service;

import java.util.List;

import com.oga.springboot.app.usuarios.models.entity.Unidades;

public interface IUnidadesService {
	public List<Unidades> findUnidadesAll();
	public Unidades findUnidadesById(Integer id);
	public Unidades saveUnidades(Unidades unidades);
	public void deleteUnidadesById(Integer id);

}
