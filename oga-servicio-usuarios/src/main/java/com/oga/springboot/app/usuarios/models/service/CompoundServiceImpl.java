package com.oga.springboot.app.usuarios.models.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.oga.springboot.app.usuarios.models.dao.FuerosDao;
import com.oga.springboot.app.usuarios.models.dao.NivelesDao;
import com.oga.springboot.app.usuarios.models.dao.PerfilesDao;
import com.oga.springboot.app.usuarios.models.dao.UnidadesDao;
import com.oga.springboot.app.usuarios.models.dao.UsuariosDao;
import com.oga.springboot.app.usuarios.models.entity.Fueros;
import com.oga.springboot.app.usuarios.models.entity.Niveles;
import com.oga.springboot.app.usuarios.models.entity.Perfiles;
import com.oga.springboot.app.usuarios.models.entity.Unidades;
import com.oga.springboot.app.usuarios.models.entity.Usuarios;
import com.oga.springboot.app.commons.models.entity.Ofijus;
import com.oga.springboot.app.usuarios.clients.OfijusClienteRest;

@Service
@Primary
public class CompoundServiceImpl implements ICompoundService{
	
	@Autowired
	private OfijusClienteRest clienteFeign;

	@Autowired
	private UsuariosDao usuariosDao;
	
	@Autowired
	private UnidadesDao unidadesDao;
	
	@Autowired
	private PerfilesDao perfilesDao;
	
	@Autowired
	private NivelesDao nivelesDao;
	
	@Autowired
	private FuerosDao fuerosDao;

	@Override
	@Transactional(readOnly=true)
	public List<Usuarios> findUsuariosAll() {
		return (List<Usuarios>) usuariosDao.findAll();
	}

	@Override
	public Usuarios findUsuariosById(Integer id) {
		return usuariosDao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public Usuarios saveUsuarios(Usuarios usuario) {
		return usuariosDao.save(usuario);
	}

	@Override
	@Transactional
	public void deleteUsuariosById(Integer id) {
		usuariosDao.deleteById(id);
		
	}

	@Override
	@Transactional(readOnly=true)
	public List<Unidades> findUnidadesAll() {
		return (List<Unidades>) unidadesDao.findAll();
	}

	@Override
	public Unidades findUnidadesById(Integer id) {
		return unidadesDao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public Unidades saveUnidades(Unidades unidades) {
		return unidadesDao.save(unidades);
	}

	@Override
	@Transactional
	public void deleteUnidadesById(Integer id) {
		unidadesDao.deleteById(id);
	}

	@Override
	@Transactional(readOnly=true)
	public List<Perfiles> findPerfilesAll() {
		return (List<Perfiles>) perfilesDao.findAll();
	}

	@Override
	public Perfiles findPerfilesById(Integer id) {
		return perfilesDao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public Perfiles savePerfiles(Perfiles perfil) {
		return perfilesDao.save(perfil);
	}

	@Override
	@Transactional
	public void deletePerfilesById(Integer id) {
		perfilesDao.deleteById(id);
	}

	@Override
	@Transactional(readOnly=true)
	public List<Niveles> findNivelesAll() {
		return (List<Niveles>) nivelesDao.findAll();
	}

	@Override
	public Niveles findNivelesById(Integer id) {
		return nivelesDao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public Niveles saveNiveles(Niveles nivel) {
		return nivelesDao.save(nivel);
	}

	@Override
	@Transactional
	public void deleteNivelesById(Integer id) {
		nivelesDao.deleteById(id);
		
	}

	@Override
	@Transactional(readOnly=true)
	public List<Fueros> findFuerosAll() {
		return (List<Fueros>) fuerosDao.findAll();
	}

	@Override
	public Fueros findFuerosById(Integer id) {
		return fuerosDao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public Fueros saveFueros(Fueros fuero) {
		return fuerosDao.save(fuero);
	}

	@Override
	@Transactional
	public void deleteFuerosById(Integer id) {
		fuerosDao.deleteById(id);
	}

	@Override
	public Usuarios findCompleteUsuarioById(Integer id) {
		Usuarios usuario = usuariosDao.findById(id).orElse(null);
		if (usuario!=null && usuario.getOfijuid()!=null) {
			Ofijus ofijus=clienteFeign.detalle(usuario.getOfijuid());
			usuario.setOfijus(ofijus);
		}
		return usuario;
	}
}
