package com.oga.springboot.app.usuarios.models.service;

public interface ICompoundService
		extends IUsuariosService, IUnidadesService, IPerfilesService, 
		INivelesService, IFuerosService {

}
