package com.oga.springboot.app.usuarios.models.service;

import java.util.List;

import com.oga.springboot.app.usuarios.models.entity.Perfiles;

public interface IPerfilesService {

	public List<Perfiles> findPerfilesAll();
	public Perfiles findPerfilesById(Integer id);
	public Perfiles savePerfiles(Perfiles perfil);
	public void deletePerfilesById(Integer id);
}
