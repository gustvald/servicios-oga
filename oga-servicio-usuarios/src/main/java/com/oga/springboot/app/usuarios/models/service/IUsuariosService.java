package com.oga.springboot.app.usuarios.models.service;

import java.util.List;

import com.oga.springboot.app.usuarios.models.entity.Usuarios;

public interface IUsuariosService {
	public List<Usuarios> findUsuariosAll();
	public Usuarios findUsuariosById(Integer id);
	public Usuarios saveUsuarios(Usuarios usuarios);
	public void deleteUsuariosById(Integer id);
	public Usuarios findCompleteUsuarioById(Integer id);
	
}
