package com.oga.springboot.app.usuarios.models.dao;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.oga.springboot.app.usuarios.models.entity.Unidades;


public interface UnidadesDao extends PagingAndSortingRepository<Unidades, Integer>{

}
