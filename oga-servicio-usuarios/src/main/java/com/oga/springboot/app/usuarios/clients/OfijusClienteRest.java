package com.oga.springboot.app.usuarios.clients;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.oga.springboot.app.commons.models.entity.Ofijus;


@FeignClient(name = "servicio-ogasalas")
public interface OfijusClienteRest {
	@GetMapping("/listar-ofijus")
	public List<Ofijus> listar();
	
	@GetMapping("/ver-ofijus/{id}")
	public Ofijus detalle(@PathVariable Integer id);
	
	@PostMapping("/crear-ofijus")
	public Ofijus crear(@RequestBody Ofijus ofijus);
	
	@PutMapping("/editar-ofijus/{id}")
	public Ofijus update(@RequestBody Ofijus ofijus, @PathVariable Integer id);
	
	@DeleteMapping("/eliminar-ofijus/{id}")
	public void eliminar(@PathVariable Integer id);
}
