package com.oga.springboot.app.usuarios.models.dao;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.oga.springboot.app.usuarios.models.entity.Usuarios;


public interface UsuariosDao extends PagingAndSortingRepository<Usuarios, Integer>{

}
