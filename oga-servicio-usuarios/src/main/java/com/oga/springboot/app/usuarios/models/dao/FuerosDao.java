package com.oga.springboot.app.usuarios.models.dao;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.oga.springboot.app.usuarios.models.entity.Fueros;

public interface FuerosDao extends PagingAndSortingRepository<Fueros, Integer>{

}
