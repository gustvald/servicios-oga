package com.oga.springboot.app.usuarios.models.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.oga.springboot.app.commons.models.entity.Ofijus;

@Entity
@Table(name = "usuarios")
public class Usuarios implements Serializable {

	private static final long serialVersionUID = 4936741681666585779L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(nullable = false)
	private Integer id;
	@Basic(optional = false)
	@Column(nullable = false, length = 120)
	private String apellido;
	@Basic(optional = false)
	@Column(nullable = false, length = 120)
	private String nombre;
	// @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?",
	// message="Invalid email")//if the field contains email address consider using
	// this annotation to enforce field validation
	@Basic(optional = false)
	@Column(nullable = false, length = 255)
	private String email;
	@Basic(optional = false)
	@Column(nullable = false, length = 255)
	private String password;
	@Basic(optional = false)
	@Column(nullable = false)
	private long legajo;
	@Basic(optional = false)
	@Column(nullable = false)
	private short activo;
	@Basic(optional = false)
	@Column(nullable = false)
	private short cc;
	@Column(length = 255)
	private String token;
	@Column(name = "remember_token", length = 100)
	private String rememberToken;
	@Column(name = "created_at")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdAt;
	@Column(name = "updated_at")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedAt;
	@Basic(optional = false)
	@Column(nullable = false)
	private short privadas;
	@JoinColumn(name = "nivelid", referencedColumnName = "id", nullable = false)
	@ManyToOne(optional = false)
	private Niveles nivelid;
	private Integer ofijuid;
	@JoinColumn(name = "perfilid", referencedColumnName = "id", nullable = false)
	@ManyToOne(optional = false)
	private Perfiles perfilid;
	@JoinColumn(name = "unidadid", referencedColumnName = "id", nullable = false)
	@ManyToOne(optional = false)
	private Unidades unidadid;
	@Transient
	private Ofijus ofijus;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getApellido() {
		return apellido;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public long getLegajo() {
		return legajo;
	}
	public void setLegajo(long legajo) {
		this.legajo = legajo;
	}
	public short getActivo() {
		return activo;
	}
	public void setActivo(short activo) {
		this.activo = activo;
	}
	public short getCc() {
		return cc;
	}
	public void setCc(short cc) {
		this.cc = cc;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getRememberToken() {
		return rememberToken;
	}
	public void setRememberToken(String rememberToken) {
		this.rememberToken = rememberToken;
	}
	public Date getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}
	public Date getUpdatedAt() {
		return updatedAt;
	}
	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}
	public short getPrivadas() {
		return privadas;
	}
	public void setPrivadas(short privadas) {
		this.privadas = privadas;
	}
	public Niveles getNivelid() {
		return nivelid;
	}
	public void setNivelid(Niveles nivelid) {
		this.nivelid = nivelid;
	}
	public Integer getOfijuid() {
		return ofijuid;
	}
	public void setOfijuid(Integer ofijuid) {
		this.ofijuid = ofijuid;
	}
	public Perfiles getPerfilid() {
		return perfilid;
	}
	public void setPerfilid(Perfiles perfilid) {
		this.perfilid = perfilid;
	}
	public Unidades getUnidadid() {
		return unidadid;
	}
	public void setUnidadid(Unidades unidadid) {
		this.unidadid = unidadid;
	}
	public Ofijus getOfijus() {
		return ofijus;
	}
	public void setOfijus(Ofijus ofijus) {
		this.ofijus = ofijus;
	}
}
