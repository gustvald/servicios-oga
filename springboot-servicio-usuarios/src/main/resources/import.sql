INSERT INTO usuarios (username,password,enabled,nombre,apellido,email) VALUES ('andres','$2a$10$MEd6v8dap639c9tm9.kI1ObGbb6SITD.GwS419Rl6b88UPHGqn.Mm',true,'Andres','Guzman','profeso@bolsadeideas.com');
INSERT INTO usuarios (username,password,enabled,nombre,apellido,email) VALUES ('admin','$2a$10$E4aY/J0l2t0o4Y2BT.GeaOEE/jCsW6PpoeRNZyB9rQ2YgrgZrA.lG',true,'John','Doe','john.doe@bolsadeideas.com');

INSERT INTO roles (nombre) VALUES('ROLE_USER');
INSERT INTO roles (nombre) VALUES('ROLE_ADMIN');

INSERT INTO usuarios_roles (usuario_id,role_id) VALUES(1,1);
INSERT INTO usuarios_roles (usuario_id,role_id) VALUES(2,2);
INSERT INTO usuarios_roles (usuario_id,role_id) VALUES(2,1);