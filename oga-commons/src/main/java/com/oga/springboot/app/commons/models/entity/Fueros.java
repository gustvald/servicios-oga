package com.oga.springboot.app.commons.models.entity;

import java.io.Serializable;
import java.util.Date;

public class Fueros implements Serializable{
	private static final long serialVersionUID = 1456643756736707348L;
	private Integer id;
	private String descripcion;
	private String ip;
	private String puerto;
	private String bd;
	private short activo;
	private short visible;
	private Integer invenietid;
	private Date createdAt;
	private Date updatedAt;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public String getPuerto() {
		return puerto;
	}
	public void setPuerto(String puerto) {
		this.puerto = puerto;
	}
	public String getBd() {
		return bd;
	}
	public void setBd(String bd) {
		this.bd = bd;
	}
	public short getActivo() {
		return activo;
	}
	public void setActivo(short activo) {
		this.activo = activo;
	}
	public short getVisible() {
		return visible;
	}
	public void setVisible(short visible) {
		this.visible = visible;
	}
	public Integer getInvenietid() {
		return invenietid;
	}
	public void setInvenietid(Integer invenietid) {
		this.invenietid = invenietid;
	}
	public Date getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}
	public Date getUpdatedAt() {
		return updatedAt;
	}
	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}
	
}
