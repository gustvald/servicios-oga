package com.oga.springboot.app.commons.models.entity;

import java.io.Serializable;
import java.util.Date;

public class Audiencias implements Serializable{

	private static final long serialVersionUID = -3793319540140069016L;
	
	    private Integer id;
	 	private Integer salaId;
	    private Date inicio;
	    private Date fin;
	    private Date inicioReal;
	    private Date finReal;
	    private Integer expedienteId;
	    private String caratula;
	    private Integer unidadId;
	    private Integer estadoId;
	    private Integer prioridadId;
	    private short confirmada;
	    private String observacion;
	    private String observacionRechazo;
	    private String observacionAgendamiento;
	    private String recursos;
	    private Integer motivoId;
	    private Integer operadorId;
	    private short privada;
	    private Integer invenietId;
	    private Integer fiscaliaId;
	    private Integer fiscalId;
	    private Integer fiscalAuxiliarId;
	    private short defensorOficial;
	    private Integer usuarioId;
	    private Integer usoId;
	    private Date uso;
	    private Date createdAt;
	    private Date updatedAt;
	    private Short aux;
	    private Ofijus ofijuid;

		public Integer getId() {
			return id;
		}

		public void setId(Integer id) {
			this.id = id;
		}

		public Integer getSalaId() {
			return salaId;
		}

		public void setSalaId(Integer salaId) {
			this.salaId = salaId;
		}

		public Date getInicio() {
			return inicio;
		}

		public void setInicio(Date inicio) {
			this.inicio = inicio;
		}

		public Date getFin() {
			return fin;
		}

		public void setFin(Date fin) {
			this.fin = fin;
		}

		public Date getInicioReal() {
			return inicioReal;
		}

		public void setInicioReal(Date inicioReal) {
			this.inicioReal = inicioReal;
		}

		public Date getFinReal() {
			return finReal;
		}

		public void setFinReal(Date finReal) {
			this.finReal = finReal;
		}

		public Integer getExpedienteId() {
			return expedienteId;
		}

		public void setExpedienteId(Integer expedienteId) {
			this.expedienteId = expedienteId;
		}

		public String getCaratula() {
			return caratula;
		}

		public void setCaratula(String caratula) {
			this.caratula = caratula;
		}

		public Integer getUnidadId() {
			return unidadId;
		}

		public void setUnidadId(Integer unidadId) {
			this.unidadId = unidadId;
		}

		public Integer getEstadoId() {
			return estadoId;
		}

		public void setEstadoId(Integer estadoId) {
			this.estadoId = estadoId;
		}

		public Integer getPrioridadId() {
			return prioridadId;
		}

		public void setPrioridadId(Integer prioridadId) {
			this.prioridadId = prioridadId;
		}

		public short getConfirmada() {
			return confirmada;
		}

		public void setConfirmada(short confirmada) {
			this.confirmada = confirmada;
		}

		public String getObservacion() {
			return observacion;
		}

		public void setObservacion(String observacion) {
			this.observacion = observacion;
		}

		public String getObservacionRechazo() {
			return observacionRechazo;
		}

		public void setObservacionRechazo(String observacionRechazo) {
			this.observacionRechazo = observacionRechazo;
		}

		public String getObservacionAgendamiento() {
			return observacionAgendamiento;
		}

		public void setObservacionAgendamiento(String observacionAgendamiento) {
			this.observacionAgendamiento = observacionAgendamiento;
		}

		public String getRecursos() {
			return recursos;
		}

		public void setRecursos(String recursos) {
			this.recursos = recursos;
		}

		public Integer getMotivoId() {
			return motivoId;
		}

		public void setMotivoId(Integer motivoId) {
			this.motivoId = motivoId;
		}

		public Integer getOperadorId() {
			return operadorId;
		}

		public void setOperadorId(Integer operadorId) {
			this.operadorId = operadorId;
		}

		public short getPrivada() {
			return privada;
		}

		public void setPrivada(short privada) {
			this.privada = privada;
		}

		public Integer getInvenietId() {
			return invenietId;
		}

		public void setInvenietId(Integer invenietId) {
			this.invenietId = invenietId;
		}

		public Integer getFiscaliaId() {
			return fiscaliaId;
		}

		public void setFiscaliaId(Integer fiscaliaId) {
			this.fiscaliaId = fiscaliaId;
		}

		public Integer getFiscalId() {
			return fiscalId;
		}

		public void setFiscalId(Integer fiscalId) {
			this.fiscalId = fiscalId;
		}

		public Integer getFiscalAuxiliarId() {
			return fiscalAuxiliarId;
		}

		public void setFiscalAuxiliarId(Integer fiscalAuxiliarId) {
			this.fiscalAuxiliarId = fiscalAuxiliarId;
		}

		public short getDefensorOficial() {
			return defensorOficial;
		}

		public void setDefensorOficial(short defensorOficial) {
			this.defensorOficial = defensorOficial;
		}

		public Integer getUsuarioId() {
			return usuarioId;
		}

		public void setUsuarioId(Integer usuarioId) {
			this.usuarioId = usuarioId;
		}

		public Integer getUsoId() {
			return usoId;
		}

		public void setUsoId(Integer usoId) {
			this.usoId = usoId;
		}

		public Date getUso() {
			return uso;
		}

		public void setUso(Date uso) {
			this.uso = uso;
		}

		public Date getCreatedAt() {
			return createdAt;
		}

		public void setCreatedAt(Date createdAt) {
			this.createdAt = createdAt;
		}

		public Date getUpdatedAt() {
			return updatedAt;
		}

		public void setUpdatedAt(Date updatedAt) {
			this.updatedAt = updatedAt;
		}

		public Short getAux() {
			return aux;
		}

		public void setAux(Short aux) {
			this.aux = aux;
		}

		public Ofijus getOfijuid() {
			return ofijuid;
		}

		public void setOfijuid(Ofijus ofijuid) {
			this.ofijuid = ofijuid;
		}
	    
}
