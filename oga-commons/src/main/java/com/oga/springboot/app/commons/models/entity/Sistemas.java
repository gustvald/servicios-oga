package com.oga.springboot.app.commons.models.entity;

import java.io.Serializable;
import java.util.Date;

public class Sistemas implements Serializable {

	private static final long serialVersionUID = 8778704850592680808L;

	private Integer id;
	private String nombre;
	private String host;
	private Integer port;
	private String bdName;
	private String bdUser;
	private String bdPass;
	private Integer bdPort;
	private String api;
	private String apiHost;
	private String apiUser;
	private String apiPass;
	private String apiKey;
	private String apiVideo;
	private String token;
	private short activo;
	private Date createdAt;
	private Date updatedAt;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getHost() {
		return host;
	}
	public void setHost(String host) {
		this.host = host;
	}
	public Integer getPort() {
		return port;
	}
	public void setPort(Integer port) {
		this.port = port;
	}
	public String getBdName() {
		return bdName;
	}
	public void setBdName(String bdName) {
		this.bdName = bdName;
	}
	public String getBdUser() {
		return bdUser;
	}
	public void setBdUser(String bdUser) {
		this.bdUser = bdUser;
	}
	public String getBdPass() {
		return bdPass;
	}
	public void setBdPass(String bdPass) {
		this.bdPass = bdPass;
	}
	public Integer getBdPort() {
		return bdPort;
	}
	public void setBdPort(Integer bdPort) {
		this.bdPort = bdPort;
	}
	public String getApi() {
		return api;
	}
	public void setApi(String api) {
		this.api = api;
	}
	public String getApiHost() {
		return apiHost;
	}
	public void setApiHost(String apiHost) {
		this.apiHost = apiHost;
	}
	public String getApiUser() {
		return apiUser;
	}
	public void setApiUser(String apiUser) {
		this.apiUser = apiUser;
	}
	public String getApiPass() {
		return apiPass;
	}
	public void setApiPass(String apiPass) {
		this.apiPass = apiPass;
	}
	public String getApiKey() {
		return apiKey;
	}
	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}
	public String getApiVideo() {
		return apiVideo;
	}
	public void setApiVideo(String apiVideo) {
		this.apiVideo = apiVideo;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public short getActivo() {
		return activo;
	}
	public void setActivo(short activo) {
		this.activo = activo;
	}
	public Date getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}
	public Date getUpdatedAt() {
		return updatedAt;
	}
	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}
}
