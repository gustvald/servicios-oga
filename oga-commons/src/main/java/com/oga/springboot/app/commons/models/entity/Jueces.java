package com.oga.springboot.app.commons.models.entity;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
public class Jueces implements Serializable {

	private static final long serialVersionUID = 7966990674097994226L;

	private Integer id;
	private String apellido;
	private String nombre;
	private String email;
	private int legajo;
	private String telefono;
	private short menor;
	private short ejecucion;
	private BigInteger carga;
	private short activo;
	private Date createdAt;
	private Date updatedAt;
	private Ofijus ofijuid;
	private Usuarios userid;
	private Usuarios usuarioid;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getApellido() {
		return apellido;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public int getLegajo() {
		return legajo;
	}
	public void setLegajo(int legajo) {
		this.legajo = legajo;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public short getMenor() {
		return menor;
	}
	public void setMenor(short menor) {
		this.menor = menor;
	}
	public short getEjecucion() {
		return ejecucion;
	}
	public void setEjecucion(short ejecucion) {
		this.ejecucion = ejecucion;
	}
	public BigInteger getCarga() {
		return carga;
	}
	public void setCarga(BigInteger carga) {
		this.carga = carga;
	}
	public short getActivo() {
		return activo;
	}
	public void setActivo(short activo) {
		this.activo = activo;
	}
	public Date getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}
	public Date getUpdatedAt() {
		return updatedAt;
	}
	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}
	public Ofijus getOfijuid() {
		return ofijuid;
	}
	public void setOfijuid(Ofijus ofijuid) {
		this.ofijuid = ofijuid;
	}
	public Usuarios getUserid() {
		return userid;
	}
	public void setUserid(Usuarios userid) {
		this.userid = userid;
	}
	public Usuarios getUsuarioid() {
		return usuarioid;
	}
	public void setUsuarioid(Usuarios usuarioid) {
		this.usuarioid = usuarioid;
	}
	
}
