package com.oga.springboot.app.commons.models.entity;

import java.io.Serializable;
import java.util.Date;


public class Salas implements Serializable {

	private static final long serialVersionUID = -7250383275822544840L;

	private Integer id;
	private String descripcion;
	private String domicilio;
	private String ubicacion;
	private Integer invenietid;
	private short activo;
	private Date createdAt;
	private Date updatedAt;
	private Ofijus ofijuid;
	private Sistemas sistemaid;
//	private Usuarios usuarioid;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getDomicilio() {
		return domicilio;
	}

	public void setDomicilio(String domicilio) {
		this.domicilio = domicilio;
	}

	public String getUbicacion() {
		return ubicacion;
	}

	public void setUbicacion(String ubicacion) {
		this.ubicacion = ubicacion;
	}

	public Integer getInvenietid() {
		return invenietid;
	}

	public void setInvenietid(Integer invenietid) {
		this.invenietid = invenietid;
	}

	public short getActivo() {
		return activo;
	}

	public void setActivo(short activo) {
		this.activo = activo;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}


	public Ofijus getOfijuid() {
		return ofijuid;
	}

	public void setOfijuid(Ofijus ofijuid) {
		this.ofijuid = ofijuid;
	}

	public Sistemas getSistemaid() {
		return sistemaid;
	}

	public void setSistemaid(Sistemas sistemaid) {
		this.sistemaid = sistemaid;
	}

//	public Usuarios getUsuarioid() {
//		return usuarioid;
//	}
//
//	public void setUsuarioid(Usuarios usuarioid) {
//		this.usuarioid = usuarioid;
//	}

}
