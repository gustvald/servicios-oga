package com.oga.springboot.app.commons.models.entity;

import java.io.Serializable;
import java.util.Date;

public class Usuarios implements Serializable {

	private static final long serialVersionUID = 4692079512231113182L;

	private Integer id;
	private String apellido;
	private String nombre;
	private String email;
	private String password;
	private long legajo;
	private short activo;
	private short cc;
	private String token;
	private String rememberToken;
	private Date createdAt;
	private Date updatedAt;
	private short privadas;
	private Niveles nivelid;
	private Integer ofijuid;
	private Perfiles perfilid;
	private Unidades unidadid;
	private Ofijus ofijus;
	

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getApellido() {
		return apellido;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public long getLegajo() {
		return legajo;
	}
	public void setLegajo(long legajo) {
		this.legajo = legajo;
	}
	public short getActivo() {
		return activo;
	}
	public void setActivo(short activo) {
		this.activo = activo;
	}
	public short getCc() {
		return cc;
	}
	public void setCc(short cc) {
		this.cc = cc;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getRememberToken() {
		return rememberToken;
	}
	public void setRememberToken(String rememberToken) {
		this.rememberToken = rememberToken;
	}
	public Date getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}
	public Date getUpdatedAt() {
		return updatedAt;
	}
	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}
	public short getPrivadas() {
		return privadas;
	}
	public void setPrivadas(short privadas) {
		this.privadas = privadas;
	}
	public Integer getOfijuid() {
		return ofijuid;
	}
	public void setOfijuid(Integer ofijuid) {
		this.ofijuid = ofijuid;
	}

}
