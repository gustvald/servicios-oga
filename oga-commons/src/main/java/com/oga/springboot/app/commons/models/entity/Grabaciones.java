package com.oga.springboot.app.commons.models.entity;

import java.io.Serializable;
import java.util.Date;

public class Grabaciones implements Serializable {

	private static final long serialVersionUID = 4804844865785016474L;

	private Integer id;
	private String ruta;
	private String archivo;
	private Date inicio;
	private Date fin;
	private Date createdAt;
	private Date updatedAt;
	private Integer audienciaid;
	private Salas salaid;
	private Integer usuarioid;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getRuta() {
		return ruta;
	}
	public void setRuta(String ruta) {
		this.ruta = ruta;
	}
	public String getArchivo() {
		return archivo;
	}
	public void setArchivo(String archivo) {
		this.archivo = archivo;
	}
	public Date getInicio() {
		return inicio;
	}
	public void setInicio(Date inicio) {
		this.inicio = inicio;
	}
	public Date getFin() {
		return fin;
	}
	public void setFin(Date fin) {
		this.fin = fin;
	}
	public Date getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}
	public Date getUpdatedAt() {
		return updatedAt;
	}
	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}
	public Integer getAudienciaid() {
		return audienciaid;
	}
	public void setAudienciaid(Integer audienciaid) {
		this.audienciaid = audienciaid;
	}
	public Salas getSalaid() {
		return salaid;
	}
	public void setSalaid(Salas salaid) {
		this.salaid = salaid;
	}
	public Integer getUsuarioid() {
		return usuarioid;
	}
	public void setUsuarioid(Integer usuarioid) {
		this.usuarioid = usuarioid;
	}

}
