package com.oga.springboot.app.commons.models.entity;

import java.io.Serializable;
import java.util.Date;


public class Unidades implements Serializable{
	private static final long serialVersionUID = 1252957269864790273L;
	private Integer id;
	private String descripcion;
	private Short nominacion;
	private String email;
	private Integer saeid;
	private Integer invenietid;
	private short activo;
	private Date createdAt;
	private Date updatedAt;
	private Fueros fueroid;
	private Perfiles perfilid;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public Short getNominacion() {
		return nominacion;
	}
	public void setNominacion(Short nominacion) {
		this.nominacion = nominacion;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Integer getSaeid() {
		return saeid;
	}
	public void setSaeid(Integer saeid) {
		this.saeid = saeid;
	}
	public Integer getInvenietid() {
		return invenietid;
	}
	public void setInvenietid(Integer invenietid) {
		this.invenietid = invenietid;
	}
	public short getActivo() {
		return activo;
	}
	public void setActivo(short activo) {
		this.activo = activo;
	}
	public Date getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}
	public Date getUpdatedAt() {
		return updatedAt;
	}
	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}
	public Fueros getFueroid() {
		return fueroid;
	}
	public void setFueroid(Fueros fueroid) {
		this.fueroid = fueroid;
	}
	public Perfiles getPerfilid() {
		return perfilid;
	}
	public void setPerfilid(Perfiles perfilid) {
		this.perfilid = perfilid;
	}
	
	
}
