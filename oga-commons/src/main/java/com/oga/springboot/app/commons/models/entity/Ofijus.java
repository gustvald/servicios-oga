package com.oga.springboot.app.commons.models.entity;

import java.io.Serializable;
import java.util.Date;
public class Ofijus implements Serializable{

	private static final long serialVersionUID = -95134098184167761L;
	
    private Integer id;
    private String nombre;
    private String direccion;
    private String localidad;
    private String telefono;
    private String telefonoAlt;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    private String email;
    private String emailAgenda;
    private String observaciones;
    private int carga;
    private Date ultimaAsignacion;
    private short activo;
    private Date createdAt;
    private Date updatedAt;
    
    
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getLocalidad() {
		return localidad;
	}
	public void setLocalidad(String localidad) {
		this.localidad = localidad;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public String getTelefonoAlt() {
		return telefonoAlt;
	}
	public void setTelefonoAlt(String telefonoAlt) {
		this.telefonoAlt = telefonoAlt;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getEmailAgenda() {
		return emailAgenda;
	}
	public void setEmailAgenda(String emailAgenda) {
		this.emailAgenda = emailAgenda;
	}
	public String getObservaciones() {
		return observaciones;
	}
	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}
	public int getCarga() {
		return carga;
	}
	public void setCarga(int carga) {
		this.carga = carga;
	}
	public Date getUltimaAsignacion() {
		return ultimaAsignacion;
	}
	public void setUltimaAsignacion(Date ultimaAsignacion) {
		this.ultimaAsignacion = ultimaAsignacion;
	}
	public short getActivo() {
		return activo;
	}
	public void setActivo(short activo) {
		this.activo = activo;
	}
	public Date getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}
	public Date getUpdatedAt() {
		return updatedAt;
	}
	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}
	
}
