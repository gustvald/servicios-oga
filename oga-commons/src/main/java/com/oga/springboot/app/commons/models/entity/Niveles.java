package com.oga.springboot.app.commons.models.entity;

import java.io.Serializable;

public class Niveles implements Serializable{
	private static final long serialVersionUID = 3336340488419258975L;
	private Integer id;
	private String descripcion;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	
}
