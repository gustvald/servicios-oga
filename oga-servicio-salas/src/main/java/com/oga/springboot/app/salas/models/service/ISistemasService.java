package com.oga.springboot.app.salas.models.service;

import java.util.List;

import com.oga.springboot.app.salas.models.entity.Sistemas;

public interface ISistemasService {
	public List<Sistemas> findSistemasAll();
	public Sistemas findSistemaById(Integer id);
	public Sistemas saveSistema(Sistemas sistema);
	public void deleteSistemaById(Integer id);
}
