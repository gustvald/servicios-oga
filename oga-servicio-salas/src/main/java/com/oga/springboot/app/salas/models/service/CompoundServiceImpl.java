package com.oga.springboot.app.salas.models.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.oga.springboot.app.commons.models.entity.Usuarios;
import com.oga.springboot.app.salas.clients.UsuariosClienteRest;
import com.oga.springboot.app.salas.models.dao.OfijusDao;
import com.oga.springboot.app.salas.models.dao.SalasDao;
import com.oga.springboot.app.salas.models.dao.SistemasDao;
import com.oga.springboot.app.salas.models.entity.Ofijus;
import com.oga.springboot.app.salas.models.entity.Salas;
import com.oga.springboot.app.salas.models.entity.Sistemas;

@Service
@Primary
public class CompoundServiceImpl implements ICompoundService{
	
	@Autowired
	private UsuariosClienteRest clienteFeign;
	
	@Autowired
	private SalasDao salasDao;
	
	@Autowired
	private OfijusDao ofijusDao;
	
	@Autowired
	private SistemasDao sistemasDao;

	@Override
	@Transactional(readOnly=true)
	public List<Salas> findSalasAll() {
		return (List<Salas>) salasDao.findAll();
	}

	@Override
	public Salas findSalaById(Integer id) {
		return salasDao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public Salas saveSala(Salas sala) {
		return salasDao.save(sala);
	}

	@Override
	@Transactional
	public void deleteSalaById(Integer id) {
		salasDao.deleteById(id);
	}

	@Override
	@Transactional(readOnly=true)
	public List<Ofijus> findOfijusAll() {
		return (List<Ofijus>) ofijusDao.findAll();
	}

	@Override
	public Ofijus findOfijusById(Integer id) {
		return ofijusDao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public Ofijus saveOfijus(Ofijus ofijus) {
		return ofijusDao.save(ofijus);
	}

	@Override
	@Transactional
	public void deleteOfijusById(Integer id) {
		ofijusDao.deleteById(id);
	}

	@Override
	@Transactional(readOnly=true)
	public List<Sistemas> findSistemasAll() {
		return (List<Sistemas>) sistemasDao.findAll();
	}

	@Override
	public Sistemas findSistemaById(Integer id) {
		return sistemasDao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public Sistemas saveSistema(Sistemas sistema) {
		return sistemasDao.save(sistema);
	}

	@Override
	@Transactional
	public void deleteSistemaById(Integer id) {
		sistemasDao.deleteById(id);
	}

	@Override
	public Salas findCompleteSalaById(Integer id) {
		Salas sala = salasDao.findById(id).orElse(null);
		if (sala!=null && sala.getUsuarioid()!=null) {
			Usuarios usuario = clienteFeign.detalle(sala.getUsuarioid());
			sala.setUsuario(usuario);
		}
		return sala;
	}
}
