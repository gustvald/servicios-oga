package com.oga.springboot.app.salas.models.dao;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.oga.springboot.app.salas.models.entity.Ofijus;



public interface OfijusDao extends PagingAndSortingRepository<Ofijus, Integer>{

}
