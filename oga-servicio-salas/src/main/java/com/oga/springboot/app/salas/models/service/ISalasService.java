package com.oga.springboot.app.salas.models.service;

import java.util.List;

import com.oga.springboot.app.salas.models.entity.Salas;


public interface ISalasService {
	public List<Salas> findSalasAll();
	public Salas findSalaById(Integer id);
	public Salas saveSala(Salas ofijus);
	public void deleteSalaById(Integer id);
	public Salas findCompleteSalaById(Integer id);
	
}
