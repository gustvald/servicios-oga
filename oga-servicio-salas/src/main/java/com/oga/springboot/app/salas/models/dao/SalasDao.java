package com.oga.springboot.app.salas.models.dao;


import org.springframework.data.repository.PagingAndSortingRepository;

import com.oga.springboot.app.salas.models.entity.Salas;


public interface SalasDao extends PagingAndSortingRepository<Salas, Integer>{

}
