package com.oga.springboot.app.salas.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.oga.springboot.app.salas.models.entity.Ofijus;
import com.oga.springboot.app.salas.models.entity.Salas;
import com.oga.springboot.app.salas.models.entity.Sistemas;
import com.oga.springboot.app.salas.models.service.ICompoundService;

@RestController
public class SalasController {
	
	@Autowired
	private ICompoundService compoundService;
	
	@GetMapping("/listar-salas")
	public List<Salas> listarSalas(){
		return compoundService.findSalasAll();
	}
	
	@GetMapping("/ver-sala/{id}")
	public Salas detalleSala(@PathVariable Integer id){
		return  compoundService.findSalaById(id);
	}
	
	@GetMapping("/ver-sala-completo/{id}")
	public Salas detalleCompletoSala(@PathVariable Integer id){
		return  compoundService.findCompleteSalaById(id);
	}
	
	
	@PostMapping("/crear-sala")
	@ResponseStatus(HttpStatus.CREATED)
	public Salas crearSala(@RequestBody Salas sala) {
		return compoundService.saveSala(sala);
	}
	
	@PutMapping("/editar-sala/{id}")
	@ResponseStatus(HttpStatus.CREATED)
	public Salas editarSala(@RequestBody Salas salas, @PathVariable Integer id) {
		Salas salasDb = compoundService.findSalaById(id);
		salasDb.setActivo(salas.getActivo());
		salasDb.setCreatedAt(salas.getCreatedAt());
		salasDb.setDescripcion(salas.getDescripcion());
		salasDb.setDomicilio(salas.getDomicilio());
		salasDb.setInvenietid(salas.getInvenietid());
		salasDb.setOfijuid(salas.getOfijuid());
		salasDb.setSistemaid(salas.getSistemaid());
		salasDb.setUbicacion(salas.getUbicacion());
		salasDb.setUpdatedAt(salas.getUpdatedAt());
//		salasDb.setUsuarioid(salas.getUsuarioid());
		return compoundService.saveSala(salasDb);
	}
	
	@DeleteMapping("/eliminar-sala/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void eliminarSala(@PathVariable Integer id) {
		compoundService.deleteSalaById(id);
	}
	
	
	@GetMapping("/listar-ofijus")
	public List<Ofijus> listarOfijus(){
		return compoundService.findOfijusAll();
	}
	
	@GetMapping("/ver-ofijus/{id}")
	public Ofijus detalleOfijus(@PathVariable Integer id){
		return  compoundService.findOfijusById(id);
	}
	
	
	@PostMapping("/crear-ofijus")
	@ResponseStatus(HttpStatus.CREATED)
	public Ofijus crearOfijus(@RequestBody Ofijus ofijus) {
		return compoundService.saveOfijus(ofijus);
	}
	
	@PutMapping("/editar-ofijus/{id}")
	@ResponseStatus(HttpStatus.CREATED)
	public Ofijus editarOfijus(@RequestBody Ofijus ofijus, @PathVariable Integer id) {
		Ofijus ofijusDb = compoundService.findOfijusById(id);
		ofijusDb.setActivo(ofijus.getActivo());
		ofijusDb.setCreatedAt(ofijus.getCreatedAt());
		ofijusDb.setCarga(ofijus.getCarga());
		ofijusDb.setDireccion(ofijus.getDireccion());
		ofijusDb.setEmail(ofijus.getEmail());
		ofijusDb.setEmailAgenda(ofijus.getEmailAgenda());
		ofijusDb.setLocalidad(ofijus.getLocalidad());
		ofijusDb.setNombre(ofijus.getNombre());
		ofijusDb.setObservaciones(ofijus.getObservaciones());
		ofijusDb.setTelefono(ofijus.getTelefono());
		ofijusDb.setTelefonoAlt(ofijus.getTelefonoAlt());
		ofijusDb.setUltimaAsignacion(ofijus.getUltimaAsignacion());
		ofijusDb.setUpdatedAt(ofijus.getUpdatedAt());
		return compoundService.saveOfijus(ofijusDb);
	}
	
	@DeleteMapping("/eliminar-ofijus/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void eliminarOfijus(@PathVariable Integer id) {
		compoundService.deleteOfijusById(id);
	}
	
	@GetMapping("/listar-sistemas")
	public List<Sistemas> listarSistemas(){
		return compoundService.findSistemasAll();
	}
	
	@GetMapping("/ver-sistemas/{id}")
	public Sistemas detalleSistemas(@PathVariable Integer id){
		return  compoundService.findSistemaById(id);
	}
	
	
	@PostMapping("/crear-sistemas")
	@ResponseStatus(HttpStatus.CREATED)
	public Sistemas crearSistemas(@RequestBody Sistemas sistema) {
		return compoundService.saveSistema(sistema);
	}
	
	@PutMapping("/editar-sistemas/{id}")
	@ResponseStatus(HttpStatus.CREATED)
	public Sistemas editarSistemas(@RequestBody Sistemas sistemas, @PathVariable Integer id) {
		Sistemas sistemasDb = compoundService.findSistemaById(id);
		return compoundService.saveSistema(sistemasDb);
	}
	
	@DeleteMapping("/eliminar-sistemas/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void eliminarSistemas(@PathVariable Integer id) {
		compoundService.deleteSistemaById(id);
	}
	
}
