package com.oga.springboot.app.salas.models.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.oga.springboot.app.commons.models.entity.Usuarios;


@Entity
@Table(name = "salas")
public class Salas implements Serializable {

	private static final long serialVersionUID = -7250383275822544840L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(nullable = false)
	private Integer id;
	@Basic(optional = false)
	@NotNull
	@Size(min = 1, max = 150)
	@Column(nullable = false, length = 150)
	private String descripcion;
	@Size(max = 120)
	@Column(length = 120)
	private String domicilio;
	@Size(max = 120)
	@Column(length = 120)
	private String ubicacion;
	private Integer invenietid;
	@Basic(optional = false)
	@NotNull
	@Column(nullable = false)
	private short activo;
	@Column(name = "created_at")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdAt;
	@Column(name = "updated_at")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedAt;
	@JoinColumn(name = "ofijuid", referencedColumnName = "id")
	@ManyToOne
	private Ofijus ofijuid;
	@JoinColumn(name = "sistemaid", referencedColumnName = "id")
	@ManyToOne
	private Sistemas sistemaid;
	private Integer usuarioid;
	@Transient
	private Usuarios usuario;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getDomicilio() {
		return domicilio;
	}

	public void setDomicilio(String domicilio) {
		this.domicilio = domicilio;
	}

	public String getUbicacion() {
		return ubicacion;
	}

	public void setUbicacion(String ubicacion) {
		this.ubicacion = ubicacion;
	}

	public Integer getInvenietid() {
		return invenietid;
	}

	public void setInvenietid(Integer invenietid) {
		this.invenietid = invenietid;
	}

	public short getActivo() {
		return activo;
	}

	public void setActivo(short activo) {
		this.activo = activo;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}


	public Ofijus getOfijuid() {
		return ofijuid;
	}

	public void setOfijuid(Ofijus ofijuid) {
		this.ofijuid = ofijuid;
	}

	public Sistemas getSistemaid() {
		return sistemaid;
	}

	public void setSistemaid(Sistemas sistemaid) {
		this.sistemaid = sistemaid;
	}

	public Integer getUsuarioid() {
		return usuarioid;
	}

	public void setUsuarioid(Integer usuarioid) {
		this.usuarioid = usuarioid;
	}

	public Usuarios getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuarios usuario) {
		this.usuario = usuario;
	}
	
	

}
