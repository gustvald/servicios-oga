package com.oga.springboot.app.salas.models.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "sistemas")
public class Sistemas implements Serializable {

	private static final long serialVersionUID = 8778704850592680808L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(nullable = false)
	private Integer id;
	@Basic(optional = false)
	@NotNull
	@Size(min = 1, max = 100)
	@Column(nullable = false, length = 100)
	private String nombre;
	@Size(max = 100)
	@Column(length = 100)
	private String host;
	private Integer port;
	@Size(max = 50)
	@Column(name = "bd_name", length = 50)
	private String bdName;
	@Size(max = 100)
	@Column(name = "bd_user", length = 100)
	private String bdUser;
	@Size(max = 100)
	@Column(name = "bd_pass", length = 100)
	private String bdPass;
	@Column(name = "bd_port")
	private Integer bdPort;
	@Size(max = 150)
	@Column(length = 150)
	private String api;
	@Size(max = 100)
	@Column(name = "api_host", length = 100)
	private String apiHost;
	@Size(max = 150)
	@Column(name = "api_user", length = 150)
	private String apiUser;
	@Size(max = 150)
	@Column(name = "api_pass", length = 150)
	private String apiPass;
	@Size(max = 200)
	@Column(name = "api_key", length = 200)
	private String apiKey;
	@Size(max = 250)
	@Column(name = "api_video", length = 250)
	private String apiVideo;
	@Size(max = 250)
	@Column(length = 250)
	private String token;
	@Basic(optional = false)
	@NotNull
	@Column(nullable = false)
	private short activo;
	@Column(name = "created_at")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdAt;
	@Column(name = "updated_at")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedAt;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getHost() {
		return host;
	}
	public void setHost(String host) {
		this.host = host;
	}
	public Integer getPort() {
		return port;
	}
	public void setPort(Integer port) {
		this.port = port;
	}
	public String getBdName() {
		return bdName;
	}
	public void setBdName(String bdName) {
		this.bdName = bdName;
	}
	public String getBdUser() {
		return bdUser;
	}
	public void setBdUser(String bdUser) {
		this.bdUser = bdUser;
	}
	public String getBdPass() {
		return bdPass;
	}
	public void setBdPass(String bdPass) {
		this.bdPass = bdPass;
	}
	public Integer getBdPort() {
		return bdPort;
	}
	public void setBdPort(Integer bdPort) {
		this.bdPort = bdPort;
	}
	public String getApi() {
		return api;
	}
	public void setApi(String api) {
		this.api = api;
	}
	public String getApiHost() {
		return apiHost;
	}
	public void setApiHost(String apiHost) {
		this.apiHost = apiHost;
	}
	public String getApiUser() {
		return apiUser;
	}
	public void setApiUser(String apiUser) {
		this.apiUser = apiUser;
	}
	public String getApiPass() {
		return apiPass;
	}
	public void setApiPass(String apiPass) {
		this.apiPass = apiPass;
	}
	public String getApiKey() {
		return apiKey;
	}
	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}
	public String getApiVideo() {
		return apiVideo;
	}
	public void setApiVideo(String apiVideo) {
		this.apiVideo = apiVideo;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public short getActivo() {
		return activo;
	}
	public void setActivo(short activo) {
		this.activo = activo;
	}
	public Date getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}
	public Date getUpdatedAt() {
		return updatedAt;
	}
	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}
}
