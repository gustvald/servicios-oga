package com.oga.springboot.app.salas.models.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "ofijus")
public class Ofijus implements Serializable{

	private static final long serialVersionUID = -95134098184167761L;
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(nullable = false)
    private Integer id;
    @Basic(optional = false)
    @Column(nullable = false, length = 100)
    private String nombre;
    @Column(length = 200)
    private String direccion;
    @Column(length = 200)
    private String localidad;
    @Column(length = 30)
    private String telefono;
    @Column(name = "telefono_alt", length = 30)
    private String telefonoAlt;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Column(length = 150)
    private String email;
    @Column(name = "email_agenda", length = 150)
    private String emailAgenda;
    @Column(length = 250)
    private String observaciones;
    @Basic(optional = false)
    @Column(nullable = false)
    private int carga;
    @Column(name = "ultima_asignacion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date ultimaAsignacion;
    @Basic(optional = false)
    @Column(nullable = false)
    private short activo;
    @Column(name = "created_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;
    @Column(name = "updated_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedAt;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getLocalidad() {
		return localidad;
	}
	public void setLocalidad(String localidad) {
		this.localidad = localidad;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public String getTelefonoAlt() {
		return telefonoAlt;
	}
	public void setTelefonoAlt(String telefonoAlt) {
		this.telefonoAlt = telefonoAlt;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getEmailAgenda() {
		return emailAgenda;
	}
	public void setEmailAgenda(String emailAgenda) {
		this.emailAgenda = emailAgenda;
	}
	public String getObservaciones() {
		return observaciones;
	}
	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}
	public int getCarga() {
		return carga;
	}
	public void setCarga(int carga) {
		this.carga = carga;
	}
	public Date getUltimaAsignacion() {
		return ultimaAsignacion;
	}
	public void setUltimaAsignacion(Date ultimaAsignacion) {
		this.ultimaAsignacion = ultimaAsignacion;
	}
	public short getActivo() {
		return activo;
	}
	public void setActivo(short activo) {
		this.activo = activo;
	}
	public Date getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}
	public Date getUpdatedAt() {
		return updatedAt;
	}
	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}
	
}
