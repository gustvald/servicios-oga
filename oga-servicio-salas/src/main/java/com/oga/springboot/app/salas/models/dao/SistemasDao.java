package com.oga.springboot.app.salas.models.dao;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.oga.springboot.app.salas.models.entity.Sistemas;

public interface SistemasDao extends PagingAndSortingRepository<Sistemas, Integer>{

}
