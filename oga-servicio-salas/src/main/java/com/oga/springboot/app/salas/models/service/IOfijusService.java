package com.oga.springboot.app.salas.models.service;

import java.util.List;

import com.oga.springboot.app.salas.models.entity.Ofijus;

public interface IOfijusService {
	public List<Ofijus> findOfijusAll();
	public Ofijus findOfijusById(Integer id);
	public Ofijus saveOfijus(Ofijus ofijus);
	public void deleteOfijusById(Integer id);
}
